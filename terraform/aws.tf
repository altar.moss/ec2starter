terraform {
  backend "s3" {
    bucket  = "skylab-altarmoss"
    key     = "terraform/ec2starter"
    region  = "us-east-1"
    encrypt = "true"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 1.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}