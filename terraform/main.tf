resource "aws_security_group" "ec2starter" {
  name        = "ec2starter"
  description = "For EC2 starter"
  vpc_id      = "vpc-02cc902374a6eb101"

  ingress {
    description      = "All"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "public"
  }
}

resource "aws_instance" "app_server" {
  ami           = "ami-04902260ca3d33422"
  instance_type = "t2.micro"

  user_data = filebase64("${path.module}/userdata.sh")
  subnet_id = "subnet-068739adf7c7fcf8f"

  security_groups = [aws_security_group.ec2starter.id]

  iam_instance_profile = aws_iam_instance_profile.ec2starter.id

  tags = {
    Name = "EC2Starter"
  }
}

resource "aws_iam_role" "ec2starter_ec2" {
  name = "ec2_starter_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy" "ec2starter_ec2" {
  name = "ec2_starter_ec2_policy"
  role = aws_iam_role.ec2starter_ec2.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "*",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_instance_profile" "ec2starter" {
  name = "ec2_starter_profile"
  role = aws_iam_role.ec2starter_ec2.name
}

data "archive_file" "ec2starter" {
  type = "zip"
  source_dir = "${path.module}/package"
  output_path = "${path.module}/ec2starter.zip"
}

resource "aws_s3_bucket_object" "file_upload" {
  bucket = "skylab-altarmoss"
  key    = "ec2starter/ec2starter.zip"
  source = "${data.archive_file.ec2starter.output_path}"
}

resource "aws_ssm_association" "ec2starter" {
  name = "AWS-ConfigureAWSPackage"

  targets {
    key    = "tag:Name"
    values = ["EC2Starter"]
  }

  parameters = {
    action = "Install"
    name = "AWSCodeDeployAgent"
  }
}