resource "aws_iam_role" "ec2starter_codedeploy" {
  name = "ec2starter-codedeploy-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "codedeploy.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "AWSCodeDeployRole" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
  role       = aws_iam_role.ec2starter_codedeploy.name
}

resource "aws_codedeploy_app" "ec2starter" {
  name = "ec2starter-app"
}

resource "aws_sns_topic" "ec2starter" {
  name = "ec2starter-topic"
}

resource "aws_codedeploy_deployment_group" "ec2starter" {
  app_name              = aws_codedeploy_app.ec2starter.name
  deployment_config_name = "CodeDeployDefault.OneAtATime"
  deployment_group_name = "ec2starter-group"
  service_role_arn      = aws_iam_role.ec2starter_codedeploy.arn

  ec2_tag_set {
    ec2_tag_filter {
      key   = "Name"
      type  = "KEY_AND_VALUE"
      value = "EC2Starter"
    }
  }

  trigger_configuration {
    trigger_events     = ["DeploymentFailure"]
    trigger_name       = "ec2starter-trigger"
    trigger_target_arn = aws_sns_topic.ec2starter.arn
  }

  auto_rollback_configuration {
    enabled = true
    events  = ["DEPLOYMENT_FAILURE"]
  }

  alarm_configuration {
    alarms  = ["ec2starter-alarm"]
    enabled = true
  }
}