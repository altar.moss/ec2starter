APP_DIR="/opt/ec2starter"
chmod +x $APP_DIR/start_app.sh
cp $APP_DIR/ec2starter.service /etc/systemd/system/ec2starter.service
systemctl enable ec2starter
systemctl stop ec2starter
systemctl start ec2starter